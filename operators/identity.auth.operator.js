// Imports
const bcrypt = require('bcryptjs'); //=> https://www.npmjs.com/package/bcryptjs
const jwt = require('jsonwebtoken'); //=> https://www.npmjs.com/package/jsonwebtoken
const CryptoJS = require("crypto-js"); //=> https://www.npmjs.com/package/crypto-js
const { decryptObjectValue } = require("../ve-openapi/services/content.service");

/**
 * Register new identity from client request
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const authRegisterOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            sqlClient.query(`INSERT INTO identity SET ?`, request.sqlObject,  (error, results, fields) => {
                if (error) throw error;
                else{ return resolve( request.sqlObject ) }
            });
        } 
        catch (error) { return reject(error) }
    })
}

/**
 * Log identity from email and password
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const authLoginOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            sqlClient.query(`SELECT * FROM identity WHERE email = '${ request.body.email }'`, (error, results) => {
                if (error){ throw error }
                else if( !results[0] ){ throw `Identity not found` }
                else{
                    // Compare client body password
                    const validatedPassword = bcrypt.compareSync( 
                        request.body.password, 
                        results[0].password 
                    );
                    
                    // Check if password is valide
                    if( !validatedPassword ){ throw `Password not valid` }
                    else{
                        // Generate JWT token
                        const javascripWebToken = jwt.sign( 
                            {
                                uuid: results[0].uuid,
                                email: results[0].email,
                            }, 
                            process.env.NODE_SHARED_JWT_KEY
                        );

                        // Encrypt JWT token
                        const secureJwt = CryptoJS.AES.encrypt(javascripWebToken, process.env.NODE_SHARED_CRYPTO_KEY).toString()
        
                        // Resolve JWT crypted token
                        return resolve({
                            token: secureJwt,
                            uuid: results[0].uuid
                        })
                    }
                }
            });
        } 
        catch (error) { return reject(error) }
    })
}

/**
 * Extract identity token and associated informations
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const authTokenOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            // Resolve decoded JWT object from 'content.service' and identity associated informations
            return resolve( request.authIdentity )
        } 
        catch (error) { return reject(error) }
    })
}

// Export shared service functions
module.exports = {
    authRegisterOperator,
    authLoginOperator,
    authTokenOperator
}