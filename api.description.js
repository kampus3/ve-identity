let apiDocumentation = {
    openapi: `3.0.1`,
    info: {
        title: `Identity`,
        description: `Lorem ipsum dolor ismet`,
        contact: {
            name: `Identity Support`,
            url: `https://api.com`,
            email: `julien@dwsapp.io`
        },
        license: {
            name: `Apache 2.0`,
            url: `https://www.apache.org/licenses/LICENSE-2.0.html`
        },
        version: `0.0.1`,
    },
    servers: [
        {  
            url:`https://identity.com`,
            description: `Production server`,
            variables: {
                basePath: { 
                    default: `v1`
                }
            }
        }
    ],
    components: {
        securitySchemes: {
            Bearer: {
                type: `token`,
                description: `JWT avaible generated token (60 days)`,
                in: `header`,
            },
        },
        schemas: {
            login: {
                mandatory: { 
                    email: { type: `email`, description: `Unique email associated to identity` },
                    password: { type: `password`, description: `Secret password associated to identity` },
                },
                unique: ['email'],
                hashed: ['password'],
            }
        }
    },
    paths: {
        [`/v1`]: {
            get: {
                security: [  ],
                summary: `Get root API path`,
                operationId: `displayDescriptionOperation`,
                model: 'description',
                requestBody:{
                    required: false,
                },
                responses: {
                    ['200']:{ description: `Request succeed` },
                }
            },
        },
        [`/v1/auth/register`]: {
            post: {
                security: [],
                summary: `Connect user with email and password`,
                operationId: `authRegisterOperator`,
                model: 'identity',
                requestBody:{
                    required: true,
                    content: `application/json`,
                    type: `object`,
                    mandatory: { 
                        email: { type: `email`, description: `Unique email associated to identity` },
                        password: { type: `password`, description: `Secret password associated to identity` },
                    },
                    unique: ['email'],
                },
                responses: {
                    ['200']:{ description: `Request succeed` },
                    ['401']:{ description: `Unauthorized` },
                }
            },
        },
        [`/v1/auth/login`]: {
            post: {
                security: [],
                summary: `Connect user with email and password`,
                operationId: `authLoginOperator`,
                model: 'identity',
                requestBody:{
                    required: true,
                    content: `application/json`,
                    type: `object`,
                    mandatory: { 
                        email: { type: `email`, description: `Unique email associated to identity` },
                        password: { type: `password`, description: `Secret password associated to identity` },
                    },
                    unique: ['email'],
                    hashed: ['password'],
                },
                responses: {
                    ['200']:{ description: `Request succeed` },
                    ['401']:{ description: `Unauthorized` },
                }
            },
        },
        [`/v1/auth/token`]: {
            get: {
                security: [ `Bearer` ],
                summary: `Secure user connection with PKCE strategy`,
                operationId: `authTokenOperator`,
                model: 'token',
                requestBody:{
                    required: false,
                },
                responses: {
                    ['200']:{ description: `Request succeed` },
                    ['401']:{ description: `Unauthorized` },
                }
            },
        },
    }
};

// Export API description
module.exports = apiDocumentation;